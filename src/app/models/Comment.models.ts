export class Comment {
  commentId: number;
  text: string;
  creationDate: any;
  userName: string;
  babbleId: number;
}
