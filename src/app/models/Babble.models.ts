export class Babble {
  babbleId: number;
  text: any;
  creationDate: any;
  creator: string;
  numberOfLike: number;
  numberOfDislike: number;
  numberOfRebabble: number;
  numberOfComment: number;
  hasCurrentUserLiked: boolean;
  hasCurrentUserDisliked: boolean;
  rebabble: boolean;
  rebabbler: string;
}
