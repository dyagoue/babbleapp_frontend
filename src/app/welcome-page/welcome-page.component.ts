import { Component, OnInit } from '@angular/core';
import {Router} from '@angular/router';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {AuthService} from '../services/auth.service';
import {BabblesService} from '../services/babbles.service';
import {UsersService} from '../services/users.service';
import {Subscription} from 'rxjs';
import {UserResponse} from '../models/UserResponse.models';
import {LoginRequest} from '../models/LoginRequest.models';

@Component({
  selector: 'app-welcome-page',
  templateUrl: './welcome-page.component.html',
  styleUrls: ['./welcome-page.component.scss']
})
export class WelcomePageComponent implements OnInit {

  signinForm: FormGroup;
  hatError: boolean;
  errorMessage: string;
  userResponse: UserResponse;
  loginRequest: LoginRequest;

  constructor(private router: Router,
              private formBuilder: FormBuilder,
              private authService: AuthService,
              private babblesService: BabblesService,
              private usersService: UsersService) { }

  ngOnInit() {
    this.initForm();
    this.hatError = false;
  }

  initForm() {
    this.signinForm = this.formBuilder.group({
      userName: ['', Validators.required],
      password: ['', Validators.required]
    });
  }

  onSubmit() {
    const userName = this.signinForm.get('userName').value;
    const password = this.signinForm.get('password').value;
    this.loginRequest = new LoginRequest();
    this.loginRequest.userName = userName;
    this.loginRequest.password = password;
    this.usersService.signin(this.loginRequest).subscribe(
      data => {
          this.userResponse = new UserResponse();
          this.userResponse.userName = userName;
          this.authService.setUser(data);
          this.router.navigate(['/babble/all']);
      }, error => {
        this.hatError = true;
        this.errorMessage = 'User account not found.';
        console.log(error);
      }
    );
  }

}
