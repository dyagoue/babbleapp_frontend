import { Component, OnInit } from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {Router} from '@angular/router';
import {AuthService} from '../../services/auth.service';
import {BabblesService} from '../../services/babbles.service';
import {UsersService} from '../../services/users.service';
import {User} from '../../models/User.models';
import {UserResponse} from '../../models/UserResponse.models';

@Component({
  selector: 'app-signup',
  templateUrl: './signup.component.html',
  styleUrls: ['./signup.component.scss']
})
export class SignupComponent implements OnInit {

  signupForm: FormGroup;
  user: User;
  hatError: boolean;
  errorMessage: string;
  userResponse: UserResponse;

  constructor(private router: Router,
              private formBuilder: FormBuilder,
              private authService: AuthService,
              private babblesService: BabblesService,
              private usersService: UsersService) { }

  ngOnInit() {
    this.initForm();
    this.hatError = false;
  }

  initForm() {
    this.signupForm = this.formBuilder.group({
      userName: ['', Validators.required],
      name: ['', Validators.required],
      password: ['', Validators.required]
    });
  }

  onSubmit() {
    const userName = this.signupForm.get('userName').value;
    const name = this.signupForm.get('name').value;
    const password = this.signupForm.get('password').value;
    this.user = new User();
    this.user.userName = userName;
    this.user.name = name;
    this.user.password = password;
    this.usersService.signup(this.user).subscribe(
      data => {
          this.userResponse = new UserResponse();
          this.userResponse.userName = userName;
          this.authService.setUser(this.userResponse);
          this.router.navigate(['/babble/all']);
      }, error => {
        this.hatError = true;
        this.errorMessage = 'Username allready used.';
        console.log(error);
      }
    );
  }
}
