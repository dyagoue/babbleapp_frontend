import { Injectable } from '@angular/core';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {Observable} from 'rxjs';
import {Babble} from '../models/Babble.models';
import {Comment} from '../models/Comment.models';
import {AuthService} from './auth.service';


@Injectable({
  providedIn: 'root'
})
export class BabblesService {

  constructor(private httpClient: HttpClient,
              private authService: AuthService) { }

  // public getAllBabbles(userName: string): Observable<any> {
  //   return this.httpClient.get(`http://localhost:8081/babble/all/${userName}`);
  // }
  //
  // public findBabbleWithNewMessageByCreator(creator: string): Observable<any> {
  //   return this.httpClient.get(`http://localhost:8081/babble/getbynewcomment/${creator}`);
  // }
  //
  // public addBabble(babble: Babble): Observable<any> {
  //   return this.httpClient.post('http://localhost:8081/babble/add', babble);
  // }
  //
  // public searchByText(text: string): Observable<any> {
  //   return this.httpClient.get(`http://localhost:8081/babble/findbytext/${text}`);
  // }
  //
  // public getBabble(id: number, userName: string): Observable<any> {
  //   return this.httpClient.get(`http://localhost:8081/babble/get/${id}/${userName}`);
  // }
  //
  // public getCommentByBabbleId(babbleId: number, currentUser: string): Observable<any> {
  //   return this.httpClient.get(`http://localhost:8081/comment/getbybabbleid/${babbleId}/${currentUser}`);
  // }
  //
  // public addComment(comment: Comment): Observable<any> {
  //   return this.httpClient.post('http://localhost:8081/comment/add', comment);
  // }
  //
  // public deleteBabble(id: number): Observable<any> {
  //   return this.httpClient.delete(`http://localhost:8081/babble/delete/${id}`);
  // }
  //
  // public updateBabble(id: number, babble: Babble): Observable<any> {
  //   return this.httpClient.put(`http://localhost:8081/babble/update/${id}`, babble);
  // }
  //
  // public rebabble(rebabbler: string, babbleId: number): Observable<any> {
  //   return this.httpClient.get(`http://localhost:8081/babble/rebabble/${rebabbler}/${babbleId}`);
  // }
  //
  // public getByCreator(creator: string, userName: string): Observable<any> {
  //   return this.httpClient.get(`http://localhost:8081/babble/getbycreator/${creator}/${userName}`);
  // }
  //
  // public countNewCommentByCreator(creator: string): Observable<any> {
  //   return this.httpClient.get(`http://localhost:8081/comment/countnewcommentbycreator/${creator}`);
  // }



  public getAllBabbles(userName: string): Observable<any> {
    return this.httpClient.get(`https://frozen-forest-99237.herokuapp.com/babble/all/${userName}`);
  }
  public findBabbleWithNewMessageByCreator(creator: string): Observable<any> {
    return this.httpClient.get(`https://frozen-forest-99237.herokuapp.com/babble/getbynewcomment/${creator}`);
  }

  public addBabble(babble: Babble): Observable<any> {
    return this.httpClient.post('https://frozen-forest-99237.herokuapp.com/babble/add', babble);
  }

  public searchByText(text: string): Observable<any> {
    return this.httpClient.get(`https://frozen-forest-99237.herokuapp.com/babble/findbytext/${text}`);
  }

  public getBabble(id: number, userName: string): Observable<any> {
    return this.httpClient.get(`https://frozen-forest-99237.herokuapp.com/babble/get/${id}/${userName}`);
  }

  public getCommentByBabbleId(babbleId: number, currentUser: string): Observable<any> {
    return this.httpClient.get(`https://frozen-forest-99237.herokuapp.com/comment/getbybabbleid/${babbleId}/${currentUser}`);
  }

  public addComment(comment: Comment): Observable<any> {
    return this.httpClient.post('https://frozen-forest-99237.herokuapp.com/comment/add', comment);
  }

  public deleteBabble(id: number): Observable<any> {
    return this.httpClient.delete(`https://frozen-forest-99237.herokuapp.com/babble/delete/${id}`);
  }

  public updateBabble(id: number, babble: Babble): Observable<any> {
    return this.httpClient.put(`https://frozen-forest-99237.herokuapp.com/babble/update/${id}`, babble);
  }

  public rebabble(rebabbler: string, babbleId: number): Observable<any> {
    return this.httpClient.get(`https://frozen-forest-99237.herokuapp.com/babble/rebabble/${rebabbler}/${babbleId}`);
  }

  public getByCreator(creator: string, userName: string): Observable<any> {
    return this.httpClient.get(`https://frozen-forest-99237.herokuapp.com/babble/getbycreator/${creator}/${userName}`);
  }
  public countNewCommentByCreator(creator: string): Observable<any> {
    return this.httpClient.get(`https://frozen-forest-99237.herokuapp.com/comment/countnewcommentbycreator/${creator}`);
  }

}
