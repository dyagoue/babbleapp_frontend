import { Injectable } from '@angular/core';
import {AuthService} from './auth.service';
import {LikeModel} from '../models/Like.models';
import {UsersService} from './users.service';
import {DislikeModels} from '../models/Dislike.models';
import {BabblesService} from './babbles.service';
import {BehaviorSubject, Observable} from 'rxjs';
import {tap} from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class ActionService {

  private likeModel: LikeModel;
  private dislikeModel: DislikeModels;
  private userName: string;
  private unreadComment = new BehaviorSubject(0);
  unreadCommentCount = this.unreadComment.asObservable();

  constructor(private authService: AuthService,
              private usersService: UsersService,
              private babblesService: BabblesService) { }

  onLike(babbleId: number) {
    this.likeModel = new LikeModel();
    this.likeModel.userName = this.authService.getUsername();
    this.likeModel.babbleId = babbleId;
    this.usersService.likeBabble(this.likeModel).subscribe(
      data => {
        // window.location.reload();
      }
    );
  }

  onDislike(babbleId: number) {
    this.dislikeModel = new DislikeModels();
    this.dislikeModel.userName = this.authService.getUsername();
    this.dislikeModel.babbleId = babbleId;
    this.usersService.dislikeBabble(this.dislikeModel).subscribe(
      data => {
        // window.location.reload();
      }
    );
  }

  onDeleteLike(babbleId: number) {
    this.userName = this.authService.getUsername();
    this.usersService.deleteLikeByUserNameAndBabbleId(this.userName, babbleId).subscribe(
      data => {
        // window.location.reload();
      }
    );
  }

  onDeleteDislike(babbleId: number) {
    this.userName = this.authService.getUsername();
    this.usersService.deleteDislikeByUserNameAndBabbleId(this.userName, babbleId).subscribe(
      data => {
        // window.location.reload();
      }
    );
  }

  onShare(babbleId: number) {
    this.userName = this.authService.getUsername();
    this.babblesService.rebabble(this.userName, babbleId).subscribe(
      data => {
        window.location.reload();
      }
    );
  }

  onDeleteBabble(babbleId: number) {
    this.babblesService.deleteBabble(babbleId).subscribe(
      data => {
        window.location.reload();
      }
    );
  }

  // getNumberOfComment() {
  //   this.babblesService.countNewCommentByCreator(this.authService.getUsername()).subscribe(
  //     data => {
  //       return data;
  //     }
  //   );
  // }

  requestNumberOfComment(): Observable<any> {
    return this.babblesService.countNewCommentByCreator(this.authService.getUsername()).pipe(tap(
      data => {
        this.setNumberOfComment(data);
      }
    ));
  }

  setNumberOfComment(data) {
    this.unreadComment.next(data);
  }
}
