import { Injectable } from '@angular/core';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {Router} from '@angular/router';
import {Observable} from 'rxjs';
import {User} from '../models/User.models';
import {LikeModel} from '../models/Like.models';
import {AuthService} from './auth.service';
import {DislikeModels} from '../models/Dislike.models';
import {LoginRequest} from '../models/LoginRequest.models';

@Injectable({
  providedIn: 'root'
})
export class UsersService {

  constructor(private httpClient: HttpClient,
              private authService: AuthService) { }

  // public signin(loginRequest: LoginRequest): Observable<any> {
  //   return this.httpClient.post('http://localhost:8081/user/login', loginRequest);
  // }
  //
  // public signup(user: User): Observable<any> {
  //   return this.httpClient.post('http://localhost:8081/user/add', user);
  // }
  //
  // public getUser(userName: string): Observable<any> {
  //   return this.httpClient.get(`http://localhost:8081/user/get/${userName}`);
  // }
  //
  // public getUserBabble(userName: string): Observable<any> {
  //   return this.httpClient.get(`http://localhost:8081/babble/getbycreator/${userName}`);
  // }
  //
  // public searchByText(text: string): Observable<any> {
  //   return this.httpClient.get(`http://localhost:8081/babble/findbytext/${text}`);
  // }
  //
  // public likeBabble(likeModel: LikeModel): Observable<any> {
  //   return this.httpClient.post(`http://localhost:8081/like/add`, likeModel);
  // }
  //
  // public dislikeBabble(dislikeModel: DislikeModels): Observable<any> {
  //   return this.httpClient.post(`http://localhost:8081/dislike/add`, dislikeModel);
  // }
  //
  // public getLike(userName: string, babbleId: number): Observable<any> {
  //   return this.httpClient.get(`http://localhost:8081/like/getbyusernameandbabbleid/${userName}/${babbleId}`);
  // }
  //
  // public deleteLikeByUserNameAndBabbleId(userName: string, babbleId: number): Observable<any> {
  //   return this.httpClient.delete(`http://localhost:8081/like/deletebyusernameandbabbleid/${userName}/${babbleId}`);
  // }
  //
  // public deleteDislikeByUserNameAndBabbleId(userName: string, babbleId: number): Observable<any> {
  //   return this.httpClient.delete(`http://localhost:8081/dislike/deletebyusernameandbabbleid/${userName}/${babbleId}`);
  // }
  //
  // public getCurrentUser() {
  //   return this.httpClient.get('http://localhost:8081/user/currentuser');
  // }
  //
  // public logout(): Observable<any> {
  //   return this.httpClient.get(`http://localhost:8081/destroy`, {responseType: 'text'});
  // }

  public signin(loginRequest: LoginRequest): Observable<any> {
    return this.httpClient.post('https://frozen-forest-99237.herokuapp.com/user/login', loginRequest);
  }

  public signup(user: User): Observable<any> {
    return this.httpClient.post('https://frozen-forest-99237.herokuapp.com/user/add', user);
  }

  public getUser(userName: string): Observable<any> {
    return this.httpClient.get(`https://frozen-forest-99237.herokuapp.com/user/get/${userName}`);
  }

  public getUserBabble(userName: string): Observable<any> {
    return this.httpClient.get(`https://frozen-forest-99237.herokuapp.com/babble/getbycreator/${userName}`);
  }

  public searchByText(text: string): Observable<any> {
    return this.httpClient.get(`https://frozen-forest-99237.herokuapp.com/babble/findbytext/${text}`);
  }

  public likeBabble(likeModel: LikeModel): Observable<any> {
    return this.httpClient.post(`https://frozen-forest-99237.herokuapp.com/like/add`, likeModel);
  }

  public dislikeBabble(dislikeModel: DislikeModels): Observable<any> {
    return this.httpClient.post(`https://frozen-forest-99237.herokuapp.com/dislike/add`, dislikeModel);
  }

  public getLike(userName: string, babbleId: number): Observable<any> {
    return this.httpClient.get(`https://frozen-forest-99237.herokuapp.com/like/getbyusernameandbabbleid/${userName}/${babbleId}`);
  }

  public deleteLikeByUserNameAndBabbleId(userName: string, babbleId: number): Observable<any> {
    return this.httpClient.delete(`https://frozen-forest-99237.herokuapp.com/like/deletebyusernameandbabbleid/${userName}/${babbleId}`);
  }

  public deleteDislikeByUserNameAndBabbleId(userName: string, babbleId: number): Observable<any> {
    return this.httpClient.delete(`https://frozen-forest-99237.herokuapp.com/dislike/deletebyusernameandbabbleid/${userName}/${babbleId}`);
  }

  public getCurrentUser() {
    return this.httpClient.get('https://frozen-forest-99237.herokuapp.com/user/currentuser');
  }

  public logout(): Observable<any> {
    return this.httpClient.get(`https://frozen-forest-99237.herokuapp.com/destroy`, {responseType: 'text'});
  }
}
