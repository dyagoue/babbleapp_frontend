import { Injectable } from '@angular/core';
import {Router} from '@angular/router';
import {UserResponse} from '../models/UserResponse.models';

@Injectable({
  providedIn: 'root'
})
export class AuthService {

  constructor(private router: Router) { }

  public isAuth() {
    const data = localStorage.getItem('session') ? JSON.parse(localStorage.getItem('session')) : null;
    return data !== null;
  }

  public setUser(userResponse: UserResponse) {
    localStorage.setItem('session', JSON.stringify(userResponse));
  }

  public getUsername() {
    if (this.isAuth()) {
      return JSON.parse(localStorage.getItem('session')).userName;
    }
  }
}
