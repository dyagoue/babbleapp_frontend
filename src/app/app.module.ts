import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import * as $ from 'jquery';

import { AppComponent } from './app.component';
import { SignupComponent } from './auth/signup/signup.component';
import { SigninComponent } from './auth/signin/signin.component';
import { BabbleListComponent } from './babble-list/babble-list.component';
import { SingleBabbleComponent } from './babble-list/single-babble/single-babble.component';
import { BabbleFormComponent } from './babble-list/babble-form/babble-form.component';
import { HeaderComponent } from './header/header.component';
import { UserProfileComponent } from './user-profile/user-profile.component';
import { CommentPageComponent } from './comment-page/comment-page.component';
import { SearchPageComponent } from './search-page/search-page.component';
import { WelcomePageComponent } from './welcome-page/welcome-page.component';
import { UpdateBabbleComponent } from './update-babble/update-babble.component';
import {AuthService} from './services/auth.service';
import {AuthGuardService} from './services/auth-guard.service';
import {BabblesService} from './services/babbles.service';
import {UsersService} from './services/users.service';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {HttpClientModule} from '@angular/common/http';
import {RouterModule, Routes} from '@angular/router';
import { DeletePageComponent } from './delete-page/delete-page.component';
import { NotificationPageComponent } from './notification-page/notification-page.component';

const appRoutes: Routes = [
  { path: 'babble/all', canActivate: [AuthGuardService], component: BabbleListComponent },
  { path: 'babble/search/:text', canActivate: [AuthGuardService], component: SearchPageComponent },
  { path: 'babble/add', canActivate: [AuthGuardService], component: BabbleFormComponent },
  { path: 'babble/comment/:id', canActivate: [AuthGuardService], component: CommentPageComponent },
  { path: 'babble/update/:id', canActivate: [AuthGuardService], component: UpdateBabbleComponent },
  { path: 'babble/delete/:id', canActivate: [AuthGuardService], component: DeletePageComponent },
  { path: 'user/:userName', canActivate: [AuthGuardService], component: UserProfileComponent },
  { path: 'notification/:userName', canActivate: [AuthGuardService], component: NotificationPageComponent },
  { path: 'auth/signup', component: SignupComponent },
  { path: 'auth/signin', component: WelcomePageComponent },
  { path: '**', redirectTo: 'babble/all' }
];

@NgModule({
  declarations: [
    AppComponent,
    SignupComponent,
    SigninComponent,
    BabbleListComponent,
    SingleBabbleComponent,
    BabbleFormComponent,
    HeaderComponent,
    UserProfileComponent,
    CommentPageComponent,
    SearchPageComponent,
    WelcomePageComponent,
    UpdateBabbleComponent,
    DeletePageComponent,
    NotificationPageComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    ReactiveFormsModule,
    HttpClientModule,
    RouterModule.forRoot(appRoutes)
  ],
  providers: [
    AuthService,
    AuthGuardService,
    BabblesService,
    UsersService,
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
