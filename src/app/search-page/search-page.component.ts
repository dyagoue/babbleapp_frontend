import { Component, OnInit } from '@angular/core';
import {UsersService} from '../services/users.service';
import {ActivatedRoute} from '@angular/router';
import {BabblesService} from '../services/babbles.service';

@Component({
  selector: 'app-search-page',
  templateUrl: './search-page.component.html',
  styleUrls: ['./search-page.component.scss']
})
export class SearchPageComponent implements OnInit {

  text: string;
  babbleList: any[];

  constructor(private usersService: UsersService,
              private babblesService: BabblesService,
              private route: ActivatedRoute) { }

  ngOnInit() {
    this.text = this.route.snapshot.paramMap.get('text');
    this.babblesService.searchByText(this.text).subscribe(
      data => {
        this.babbleList = data;
      }
    );
  }

}
