import { Component, OnInit } from '@angular/core';
import {Babble} from '../models/Babble.models';
import {Comment} from '../models/Comment.models';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {UsersService} from '../services/users.service';
import {BabblesService} from '../services/babbles.service';
import {ActivatedRoute, Router} from '@angular/router';
import {AuthService} from '../services/auth.service';
import {ActionService} from '../services/action.service';

@Component({
  selector: 'app-comment-page',
  templateUrl: './comment-page.component.html',
  styleUrls: ['./comment-page.component.scss']
})
export class CommentPageComponent implements OnInit {

  commentList: any[];
  babble: Babble;
  id;
  commentForm: FormGroup;
  comment: Comment;

  constructor(private usersService: UsersService,
              private formBuilder: FormBuilder,
              private babblesService: BabblesService,
              private route: ActivatedRoute,
              private authService: AuthService,
              private actionService: ActionService) { }

  ngOnInit() {
    this.id = this.route.snapshot.paramMap.get('id');
    this.getComments();
    this.commentForm = this.formBuilder.group({
      text: ['', Validators.required]
    });
    this.getBabble();
    this.actionService.requestNumberOfComment().subscribe(
      d => {
        this.actionService.setNumberOfComment(d);
      }
    );
  }

  getBabble() {
    this.babblesService.getBabble(this.id, this.authService.getUsername()).subscribe(
      data => {
        this.babble = data;
      }
    );
  }

  getComments() {
    this.babblesService.getCommentByBabbleId(this.id, this.authService.getUsername()).subscribe(
      data => {
        this.commentList = data;
        this.actionService.requestNumberOfComment().subscribe(
          d => {
            this.actionService.setNumberOfComment(d);
          }
        );
      }
    );
  }

  onSubmit() {
    const text = this.commentForm.get('text').value;
    this.comment = new Comment();
    this.comment.text = text;
    this.comment.userName = this.authService.getUsername();
    this.comment.babbleId = this.id;
    this.babblesService.addComment(this.comment).subscribe(
      data => {
          window.location.reload();
          this.actionService.requestNumberOfComment().subscribe(
          d => {
            this.actionService.setNumberOfComment(d);
          }
        );
      }
    );
  }

  onLike(babbleId: number) {
    this.actionService.onLike(babbleId);
    this.getBabble();
    this.getBabble();
    this.actionService.requestNumberOfComment().subscribe(
      d => {
        this.actionService.setNumberOfComment(d);
      }
    );
  }

  onDeleteLike(babbleId: number) {
    this.actionService.onDeleteLike(babbleId);
    this.getBabble();
    this.getBabble();
    this.actionService.requestNumberOfComment().subscribe(
      d => {
        this.actionService.setNumberOfComment(d);
      }
    );
  }

  onDislike(babbleId: number) {
    this.actionService.onDislike(babbleId);
    this.getBabble();
    this.getBabble();
    this.actionService.requestNumberOfComment().subscribe(
      d => {
        this.actionService.setNumberOfComment(d);
      }
    );
  }

  onDeleteDislike(babbleId: number) {
    this.actionService.onDeleteDislike(babbleId);
    this.getBabble();
    this.getBabble();
    this.actionService.requestNumberOfComment().subscribe(
      d => {
        this.actionService.setNumberOfComment(d);
      }
    );
  }

  onShare(babbleId: number) {
    this.actionService.onShare(babbleId);
    // this.getAllBabbles();
  }

}
