import { Component, OnInit } from '@angular/core';
import {ActivatedRoute} from '@angular/router';
import {UsersService} from '../services/users.service';
import {User} from '../models/User.models';
import {BabblesService} from '../services/babbles.service';
import {AuthService} from '../services/auth.service';
import {ActionService} from '../services/action.service';

@Component({
  selector: 'app-user-profile',
  templateUrl: './user-profile.component.html',
  styleUrls: ['./user-profile.component.scss']
})
export class UserProfileComponent implements OnInit {
  babblesList: any[];
  user: User;
  userName;

  constructor(private route: ActivatedRoute,
              private usersService: UsersService,
              private babblesService: BabblesService,
              private authService: AuthService,
              private actionService: ActionService) { }

  ngOnInit() {
    this.userName = this.route.snapshot.paramMap.get('userName');
    this.getUser();
    this.getBabbleByUserName();
    this.actionService.requestNumberOfComment().subscribe(
      d => {
        this.actionService.setNumberOfComment(d);
      }
    );
  }

  getUser() {
    this.usersService.getUser(this.userName).subscribe(
      data => {
        this.user = data;
      }
    );
  }

  getBabbleByUserName() {
    this.babblesService.getByCreator(this.userName, this.authService.getUsername()).subscribe(
      data => {
        this.babblesList = data;
      }
    );
  }

  onLike(babbleId: number) {
    this.actionService.onLike(babbleId);
    this.getBabbleByUserName();
    this.getBabbleByUserName();
    this.actionService.requestNumberOfComment().subscribe(
      d => {
        this.actionService.setNumberOfComment(d);
      }
    );
  }

  onDeleteLike(babbleId: number) {
    this.actionService.onDeleteLike(babbleId);
    this.getBabbleByUserName();
    this.getBabbleByUserName();
    this.actionService.requestNumberOfComment().subscribe(
      d => {
        this.actionService.setNumberOfComment(d);
      }
    );
  }

  onDislike(babbleId: number) {
    this.actionService.onDislike(babbleId);
    this.getBabbleByUserName();
    this.getBabbleByUserName();
    this.actionService.requestNumberOfComment().subscribe(
      d => {
        this.actionService.setNumberOfComment(d);
      }
    );
  }

  onDeleteDislike(babbleId: number) {
    this.actionService.onDeleteDislike(babbleId);
    this.getBabbleByUserName();
    this.getBabbleByUserName();
    this.actionService.requestNumberOfComment().subscribe(
      d => {
        this.actionService.setNumberOfComment(d);
      }
    );
  }

  onShare(babbleId: number) {
    this.actionService.onShare(babbleId);
  }

}
