import { Component, OnInit } from '@angular/core';
import {BabblesService} from '../services/babbles.service';
import * as $ from 'jquery';
import {Observable} from 'rxjs';
import {LikeModel} from '../models/Like.models';
import {AuthService} from '../services/auth.service';
import {UsersService} from '../services/users.service';
import {ActionService} from '../services/action.service';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {Babble} from '../models/Babble.models';
import {Router} from '@angular/router';


@Component({
  selector: 'app-babble-list',
  templateUrl: './babble-list.component.html',
  styleUrls: ['./babble-list.component.scss']
})
export class BabbleListComponent implements OnInit {

  likeModel: LikeModel;
  babblesList: any[];
  createBabbleForm: FormGroup;
  babble: Babble;
  loading: boolean;

  constructor(private babblesService: BabblesService,
              private formBuilder: FormBuilder,
              private usersService: UsersService,
              private actionService: ActionService,
              private authService: AuthService,
              private router: Router) { }

  ngOnInit() {
    this.loading = true;
    this.initForm();
    this.getAllBabbles(this.authService.getUsername());
    this.actionService.requestNumberOfComment().subscribe(
      d => {
        this.actionService.setNumberOfComment(d);
      }
    );
  }

  initForm() {
    this.createBabbleForm = this.formBuilder.group({
      text: ['', Validators.required]
    });
  }

  getAllBabbles(userName: string) {
    userName = this.authService.getUsername();
    this.babblesService.getAllBabbles(userName).subscribe(
    data => {
      this.babblesList = data;
      this.loading = false;
      }, error => console.log(error)
    );
  }

  onSubmit() {
    this.loading = true;
    const text = this.createBabbleForm.get('text').value;
    this.babble = new Babble();
    this.babble.text = text;
    this.babble.creator = this.authService.getUsername();
    this.babblesService.addBabble(this.babble).subscribe(
      data => {
        if (data) {
          window.location.reload();
        } else { console.log('erreur'); }
      }, error => console.log(error)
    );
  }

  onLike(babbleId: number) {
    this.actionService.onLike(babbleId);
    this.getAllBabbles(this.authService.getUsername());
    this.getAllBabbles(this.authService.getUsername());
    this.actionService.requestNumberOfComment().subscribe(
      d => {
        this.actionService.setNumberOfComment(d);
      }
    );
  }

  onDeleteLike(babbleId: number) {
    this.actionService.onDeleteLike(babbleId);
    this.getAllBabbles(this.authService.getUsername());
    this.getAllBabbles(this.authService.getUsername());
    this.actionService.requestNumberOfComment().subscribe(
      d => {
        this.actionService.setNumberOfComment(d);
      }
    );
  }

  onDislike(babbleId: number) {
    this.actionService.onDislike(babbleId);
    this.getAllBabbles(this.authService.getUsername());
    this.getAllBabbles(this.authService.getUsername());
    this.actionService.requestNumberOfComment().subscribe(
      d => {
        this.actionService.setNumberOfComment(d);
      }
    );
  }

  onDeleteDislike(babbleId: number) {
    this.actionService.onDeleteDislike(babbleId);
    this.getAllBabbles(this.authService.getUsername());
    this.getAllBabbles(this.authService.getUsername());
    this.actionService.requestNumberOfComment().subscribe(
      d => {
        this.actionService.setNumberOfComment(d);
      }
    );
  }

  onShare(babbleId: number) {
    this.loading = true;
    this.actionService.onShare(babbleId);
  }
}
