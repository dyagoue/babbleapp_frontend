import { Component, OnInit } from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {Babble} from '../../models/Babble.models';
import {Router} from '@angular/router';
import {AuthService} from '../../services/auth.service';
import {BabblesService} from '../../services/babbles.service';
import {UsersService} from '../../services/users.service';
import {ActionService} from '../../services/action.service';

@Component({
  selector: 'app-babble-form',
  templateUrl: './babble-form.component.html',
  styleUrls: ['./babble-form.component.scss']
})
export class BabbleFormComponent implements OnInit {

  createBabbleForm: FormGroup;
  babble: Babble;

  constructor(private router: Router,
              private formBuilder: FormBuilder,
              private authService: AuthService,
              private babblesService: BabblesService,
              private actionService: ActionService) { }

  ngOnInit() {
    this.initForm();
    this.actionService.requestNumberOfComment().subscribe(
      d => {
        this.actionService.setNumberOfComment(d);
      }
    );
  }

  initForm() {
    this.createBabbleForm = this.formBuilder.group({
      text: ['', Validators.required]
    });
  }

  onSubmit() {
    const text = this.createBabbleForm.get('text').value;
    this.babble = new Babble();
    this.babble.text = text;
    this.babble.creator = this.authService.getUsername();
    this.babblesService.addBabble(this.babble).subscribe(
      data => {
        if (data) {
          this.router.navigate(['/babble/all']);
        } else { console.log('erreur'); }
      }, error => console.log(error)
    );
  }

}
