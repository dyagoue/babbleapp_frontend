import { Component, OnInit } from '@angular/core';
import {ActivatedRoute, Route, Router} from '@angular/router';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {AuthService} from '../services/auth.service';
import {BabblesService} from '../services/babbles.service';
import {UsersService} from '../services/users.service';
import {Babble} from '../models/Babble.models';

@Component({
  selector: 'app-update-babble',
  templateUrl: './update-babble.component.html',
  styleUrls: ['./update-babble.component.scss']
})
export class UpdateBabbleComponent implements OnInit {

  updateBabbleForm: FormGroup;
  babble: Babble;
  id;

  constructor(private router: Router,
              private route: ActivatedRoute,
              private formBuilder: FormBuilder,
              private authService: AuthService,
              private babblesService: BabblesService,
              private usersService: UsersService) { }

  ngOnInit() {
    this.id = this.route.snapshot.paramMap.get('id');
    this.babblesService.getBabble(this.id, this.authService.getUsername()).subscribe(
      data => {
        this.babble = data;
        this.updateBabbleForm.controls.text.patchValue(this.babble.text);
      }
    );

    this.updateBabbleForm = this.formBuilder.group({
      text: ['', Validators.required]
    });
  }

  onSubmit() {
    const text = this.updateBabbleForm.get('text').value;
    this.babble = new Babble();
    this.babble.text = text;
    this.babble.creator = this.authService.getUsername();
    this.babblesService.updateBabble(this.id, this.babble).subscribe(
      data => {
        this.router.navigate(['/babble/all']);
      }
    );
  }

}
