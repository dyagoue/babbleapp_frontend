import { Component, OnInit } from '@angular/core';
import {AuthService} from '../services/auth.service';
import {Router} from '@angular/router';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {BabblesService} from '../services/babbles.service';
import {UsersService} from '../services/users.service';
import {ActionService} from '../services/action.service';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss']
})
export class HeaderComponent implements OnInit {

  searchBabbleForm: FormGroup;
  numberOfNewComment;

  constructor(private router: Router,
              private formBuilder: FormBuilder,
              private authService: AuthService,
              private actionService: ActionService,
              private babblesService: BabblesService,
              private usersService: UsersService) { }

  ngOnInit() {
    this.initForm();
    // this.getNumberOfComment();
    this.actionService.requestNumberOfComment().subscribe(
      d => {
        this.actionService.setNumberOfComment(d);
      }
    );
    this.actionService.unreadCommentCount.subscribe(
      data => {
        this.numberOfNewComment = data;
        console.log(data);
      });
  }

  // getNumberOfComment() {
  //   this.babblesService.countNewCommentByCreator(this.authService.getUsername()).subscribe(
  //     data => {
  //       this.numberOfNewComment = data;
  //     }
  //   );
  // }

  initForm() {
    this.searchBabbleForm = this.formBuilder.group({
      text: ['', Validators.required]
    });
  }

  onSubmit() {
    const text = this.searchBabbleForm.get('text').value;
    this.babblesService.searchByText(text).subscribe(
      data => {
        if (data) {
          this.router.navigate([`/babble/search/${text}`]);
        }
      }
    );
  }

  public logout() {
    this.usersService.logout().subscribe(
      data => {
        localStorage.clear();
        this.router.navigate(['auth/signin']);
      }
    );
  }

}
