import { Component, OnInit } from '@angular/core';
import {User} from '../models/User.models';
import {ActivatedRoute} from '@angular/router';
import {UsersService} from '../services/users.service';
import {BabblesService} from '../services/babbles.service';
import {AuthService} from '../services/auth.service';
import {ActionService} from '../services/action.service';

@Component({
  selector: 'app-notification-page',
  templateUrl: './notification-page.component.html',
  styleUrls: ['./notification-page.component.scss']
})
export class NotificationPageComponent implements OnInit {

  babblesList: any[];
  userName;

  constructor(private route: ActivatedRoute,
              private usersService: UsersService,
              private babblesService: BabblesService,
              private authService: AuthService,
              private actionService: ActionService) { }

  ngOnInit() {
    this.userName = this.route.snapshot.paramMap.get('userName');
    this.findBabbleWithNewMessageByCreator();
    this.actionService.requestNumberOfComment().subscribe(
      d => {
        this.actionService.setNumberOfComment(d);
      }
    );
  }

  findBabbleWithNewMessageByCreator() {
    this.babblesService.findBabbleWithNewMessageByCreator(this.authService.getUsername()).subscribe(
      data => {
        this.babblesList = data;
      }
    );
  }

  onLike(babbleId: number) {
    this.actionService.onLike(babbleId);
    this.findBabbleWithNewMessageByCreator();
    this.findBabbleWithNewMessageByCreator();
    this.actionService.requestNumberOfComment().subscribe(
      d => {
        this.actionService.setNumberOfComment(d);
      }
    );
  }

  onDeleteLike(babbleId: number) {
    this.actionService.onDeleteLike(babbleId);
    this.findBabbleWithNewMessageByCreator();
    this.findBabbleWithNewMessageByCreator();
    this.actionService.requestNumberOfComment().subscribe(
      d => {
        this.actionService.setNumberOfComment(d);
      }
    );
  }

  onDislike(babbleId: number) {
    this.actionService.onDislike(babbleId);
    this.findBabbleWithNewMessageByCreator();
    this.findBabbleWithNewMessageByCreator();
    this.actionService.requestNumberOfComment().subscribe(
      d => {
        this.actionService.setNumberOfComment(d);
      }
    );
  }

  onDeleteDislike(babbleId: number) {
    this.actionService.onDeleteDislike(babbleId);
    this.findBabbleWithNewMessageByCreator();
    this.findBabbleWithNewMessageByCreator();
    this.actionService.requestNumberOfComment().subscribe(
      d => {
        this.actionService.setNumberOfComment(d);
      }
    );
  }

  onShare(babbleId: number) {
    this.actionService.onShare(babbleId);
  }

}
